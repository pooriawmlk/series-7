package sbu.cs;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Client {

    public static void main(String[] args) throws IOException {
        String filePath = args[0];

        byte[] bytes = Files.readAllBytes(Paths.get(filePath));

        ClientExecutor clientExecutor = new ClientExecutor();
        clientExecutor.setData(bytes);
        clientExecutor.writeData(filePath);
        clientExecutor.closeAll();

    }
}

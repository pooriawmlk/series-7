package sbu.cs;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerExecutor {

    private final int port =  5050;
    private ServerSocket serverSocket;
    private Socket socket;
    private DataInputStream in;
    private byte[] pdf;
    private String input;

    public ServerExecutor() throws IOException {
        serverSocket = new ServerSocket(port);
        socket = serverSocket.accept();
        in = new DataInputStream(socket.getInputStream());
    }

    public void readData() throws IOException {
        input = in.readUTF();
        int size = in.readInt();
        pdf = new byte[size];
        in.readFully(pdf);
    }

    public void createFile(String directory) throws IOException {
        File file = new File(directory);

        if(!file.exists()) {
            file.mkdir();
        }

        FileOutputStream out = new FileOutputStream(directory + "/" + input);
        out.write(pdf);
        out.close();

    }

    public void closeAll() throws IOException {
        in.close();
        serverSocket.close();
        socket.close();
    }

}

package sbu.cs;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientExecutor {

    private final int port = 5050;
    private final String ip = "localhost";
    private Socket socket;
    private DataOutputStream out;
    private byte[] data;

    public ClientExecutor() throws IOException {
        socket = new Socket(ip, port);
        out = new DataOutputStream(socket.getOutputStream());
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public void writeData(String filePath) throws IOException {
        out.writeUTF(filePath);
        out.writeInt(data.length);
        out.write(data);
    }

    public void closeAll() throws IOException {
        out.close();
        socket.close();
    }

}

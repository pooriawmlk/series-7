package sbu.cs;

import java.io.IOException;


public class Server {

    public static void main(String[] args) throws IOException {
        String directory = args[0];

        ServerExecutor serverExecutor = new ServerExecutor();
        serverExecutor.readData();
        serverExecutor.createFile(directory);
        serverExecutor.closeAll();

    }
}
